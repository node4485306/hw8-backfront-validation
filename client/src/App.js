import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import MainPage from './compoments/MainPage';
import NewsPostPage from './compoments/NewsPostPage';
import CreateNews from './compoments/CreateNews';
import EditNews from './compoments/EditNews';

// функція яка створює react застосунок
function App() {

    return (
        <Router>
            <div>
                <Routes>
                    <Route path="/" element={<MainPage />} />
                    <Route path="/post/:id" element={<NewsPostPage />} />
                    <Route path='/create-post' element={<CreateNews />} />
                    <Route path='/edit-post/:id' element={<EditNews />} />                   
                </Routes>
            </div>
        </Router>
    );
    
}

export default App;

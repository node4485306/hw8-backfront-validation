import React, { useState, useEffect } from 'react';
import { useParams, Link } from 'react-router-dom';

function NewsPostPage() {

    const { id } = useParams();   
    const [newsPost, setNewsPost] = useState([]);

    useEffect(() => {
        // Виконати запит до сервера для отримання конкретної новини за її id
        const fetchNewsPost = async () => {
            try {
                const response = await fetch(`http://localhost:8000/api/post/${id}`);
                const { data } = await response.json();

                // оновлюємо стан
                setNewsPost(data);
            } catch (error) {
                console.error('Error fetching news post:', error);
            }
        };
        fetchNewsPost();
    }, [id]);

    
    return (
        <div className='container'>
            <div className="posts-item">
                <h1>{newsPost.title}</h1>
                <p>{newsPost.content}</p>
                <div className="button-wrapper-between">
                    <Link to={'/'} className='btn'><span>&larr;</span></Link>
                    <Link to={`/edit-post/${id}`} className="btn">Редагувати новину</Link>
                </div>
                
            </div>           
            
        </div>
    );
}

export default NewsPostPage;

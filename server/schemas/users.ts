export default {
    name: 'users',
    fields: [
        {
            name: 'id',
            type: 'increments'
        },
        {
            name: 'name',
            type: 'string'
        },
        {
            name: 'email',
            type: 'string'
        },
        {
            name: 'phone',
            type: 'string'
        }
    ]
}
import { BaseController } from './base.controller';
import { Request, Response } from 'express';
import fs from 'fs'
import path from 'path'

const PostsViewPath = path.join(__dirname, "../views/posts.html")

// клас PostController є нащадком класу BaseController
export class PostController extends BaseController  {
  // конструктор класу PostController
  constructor () {
    // виклик конструктора базового класу де вказано назву таблиці з якою буде працювати контролер
    super('posts');
  }

  

  // додатковий метод для отримання сторінки з постами де ми повертаємо html
  async getPostsPage(req: Request, res: Response) {
    try {
      const html = await fs.promises.readFile(PostsViewPath, 'utf8');
      
      res.send(html);
    } catch (error) {
      console.error('Error reading file:', error);
      res.status(500).send('Internal Server Error');
    }
  }
}
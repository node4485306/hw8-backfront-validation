import { UserController } from '../controllers';
import BaseRouter from './base.routes';


// клас UsersRouter є нащадком класу BaseRouter
class UsersRouter extends BaseRouter {
    constructor() {
        // виклик конструктора базового класу BaseRouter де ми передаємо екземпляр класу UserController
        super(new UserController());
    }
}

// отримання маршрутів
const { router } = new UsersRouter();

// експорт маршрутів
export default router;

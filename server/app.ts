import express, { Application, NextFunction, Request, Response } from 'express'
import morgan from 'morgan'
import bodyParser from 'body-parser'
import { MainController, UserController, PostController } from './controllers'
import Routes from './routes'
import ViewRouter from './routes/view.routes'
import { DatabaseService } from './services'
import dotenv from 'dotenv'
import cors from 'cors'
import createPostMiddlewares from './validation/validation'
import { logRequests, logError } from './logger/logger'


const PORT = process.env.PORT || 8000

// cors опції, вказуємо, які ресурси мають доступ до сервера
const CORS_OPTIONS = {
    origin: 'http://localhost:3000',
    optionsSuccessStatus: 200
}

class App {
    private app: Application
    private mainController: MainController
    private userController: UserController
    private postController: PostController



    constructor() {
        // ініціалізація express
        this.app = express()

        this.mainController = new MainController()
        this.userController = new UserController()
        this.postController = new PostController()

    }

    // маршрутизація
    routing() {
        this.app.get('/', this.mainController.getStartPage)
        this.app.use('/', ViewRouter)
               
        this.app.post('/api/post', createPostMiddlewares, this.postController.create)

        // маршрутизація для API
        Object.keys(Routes).forEach((key) => {
            this.app.use(`/api/${key}`, Routes[key])
        })
    }

    // ініціалізація плагінів які працюються як міддлвари
    initPlugins() {
        this.app.use(bodyParser.json())
        this.app.use(morgan('dev'))
        this.app.use(cors(CORS_OPTIONS))
        this.app.use(logRequests);
        this.app.use(logError);
        
    }

    // запуск сервера
    async start() {
        if (process.env.NODE_ENV !== 'production') {
            //await DatabaseService.dropTables()
            await DatabaseService.createTables()
        }

        this.initPlugins()
        this.routing()

        this.app.listen(PORT, () => {
            console.log(`Server is running on port ${PORT}`)
        })
    }
}

// ініціалізація dotenv
dotenv.config()

// створення нашого сервера
const app = new App()

// запуск сервера
app.start()


